import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Hardware and Connection',
    Svg: require('@site/static/img/hardware.svg').default,
    description: (
      <>
        Follow these guides to help troubleshoot problems with Splats.
      </>
    ),
  },
  {
    title: 'Try the Splats Coding Coach',
    Svg: require('@site/static/img/coding-coach.svg').default,
    description: (
      <>
Your AI assistant for Unruly Splats game design and coding. An OpenAI GPT by Michael Fricano II.<br/>
        <a href="https://chat.openai.com/g/g-9QaMo7VzV-splats-coding-coach" target="new">Launch the Coach</a>
      </>
    ),
  },
  {
    title: 'Powered by Docusaurus',
    Svg: require('@site/static/img/unruly-duck.svg').default,
    description: (
      <>
        Docusaurus was designed from the ground up to be easily installed and
        used to get your website up and running quickly.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
